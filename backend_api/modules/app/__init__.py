''' flask app with mongo '''
import os
import json
import datetime
from flask import Flask
from bson.objectid import ObjectId
from flask_pymongo import PyMongo
from flask_bcrypt import Bcrypt
from flask_jwt_extended import JWTManager
from flask_cors import CORS
import json
from bson import ObjectId


class JSONEncoder(json.JSONEncoder):
    def default(self, o):
        if isinstance(o, ObjectId):
            return str(o)
        if isinstance(o, datetime.datetime):
            return str(o)
        return json.JSONEncoder.default(self, o)


# create flask object
app = Flask(__name__)
CORS(app, supports_credentials=True)

app.json_encoder = JSONEncoder
app.config['JWT_ACCESS_COOKIE_PATH'] = '/api/v1/'
app.config['JWT_REFRESH_COOKIE_PATH'] = '/api/auth/refresh'
app.config['JWT_ERROR_MESSAGE_KEY'] = 'message'
app.config['JWT_SECRET_KEY'] = os.environ.get('SECRET')
app.config['JWT_ACCESS_TOKEN_EXPIRES'] = datetime.timedelta(hours=1)
app.config['MONGO_URI'] = os.environ.get('DB')
mongo = PyMongo(app)
flask_bcrypt = Bcrypt(app)
jwt = JWTManager(app)

from app.controllers import *
