import os
import copy
from bson import ObjectId
from flask import request, jsonify
from flask_jwt_extended import (
    JWTManager, jwt_required, create_access_token,
    jwt_refresh_token_required, create_refresh_token,
    get_jwt_identity, set_access_cookies,
    set_refresh_cookies, unset_jwt_cookies
)
from datetime import datetime
from app import app, mongo, flask_bcrypt, jwt
from app.schemas.message import validate_message
import logger
import redis
r = redis.Redis.from_url(url=os.environ.get('REDIS'))

ROOT_PATH = os.environ.get('ROOT_PATH')
LOG = logger.get_root_logger(
    __name__, filename=os.path.join(ROOT_PATH, 'output.log'))

page = 25


@jwt.unauthorized_loader
def unauthorized_response(callback):
    return jsonify({
        'ok': False,
        'message': 'Missing Authorization Header'
    }), 401


@app.route('/api/v1/message/create', methods=['POST'])
@jwt_required
def createMessage():
    current_user = get_jwt_identity()
    data = validate_message(request.get_json(force=True))
    if data['ok']:
        data = data['data']
        recipient = data['recipient']
        sender = current_user['username']
        chat = {}
        chat['timestamp'] = datetime.utcnow()
        chat['users'] = [sender, recipient]
        chat['read'] = [sender]
        chat['messages'] = [{'sender': sender, 'text': data['text'],
                             'image': data['image'], 'timestamp': datetime.utcnow()}]
        _id = mongo.db.messages.insert_one(chat).inserted_id
        chat['_id'] = str(_id)
        chat['type'] = "chat"
        for user in chat['users']:
            r.lpush(user, str(chat))
        LOG.info('Created conversation ' + chat['_id'])
        # ToDo: Add message to redis, so that websocket can push
        return jsonify({'ok': True, 'data': chat}), 200
    else:
        LOG.info('Message was not sent, bad parameters: {}'.format(
            data['message']))
        return jsonify({'ok': False, 'message': 'Bad request parameters: {}'.format(data['message'])}), 400


@app.route('/api/v1/message/send', methods=['POST'])
@jwt_required
def sendMessage():
    current_user = get_jwt_identity()
    data = validate_message(request.get_json(force=True))
    if data['ok']:
        data = data['data']
        sender = current_user['username']
        message = {'_id': ObjectId(), 'sender': sender,
                   'image': data['image'], 'text': data['text'], 'timestamp': datetime.utcnow()}
        convo = mongo.db.messages.find_one_and_update({'_id': ObjectId(request.args.get('id'))},
                                                      {'$push': {
                                                          'messages': message}, '$set': {'timestamp': datetime.utcnow(), 'read': False}}
                                                      # ,{'returnNewDocument': True}
                                                      )
        LOG.info('Updated conversation ' + str(convo['_id']))
        LOG.info(str(convo['users']))
        message['_id'] = str(message['_id'])
        message['type'] = "message"
        for user in convo['users']:
            r.lpush(user, str(message))
            LOG.info('Pushed message to redis key: ' + user)
        message['_id'] = str(message['_id'])
        LOG.info('Pushed message to Redis')
        return jsonify({'ok': True, 'data': message}), 200
    else:
        LOG.info('Message was not sent, bad parameters: {}'.format(
            data['message']))
        return jsonify({'ok': False, 'message': 'Bad request parameters: {}'.format(data['message'])}), 400


@app.route('/api/v1/inbox', methods=['GET'])
@jwt_required
def inbox():
    current_user = get_jwt_identity()
    query = request.args
    data = mongo.db.messages.find(
        {'users': {'$elemMatch': {'$eq': current_user['username']}}}).sort([('timestamp', -1)])
    messages = []
    for doc in data:
        doc['_id'] = str(doc['_id'])
        messages.append(doc)
    if len(messages) == 0:
        return jsonify({'ok': True, 'data': []}), 204
    return jsonify({'ok': True, 'data': messages}), 200
