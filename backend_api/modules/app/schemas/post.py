from jsonschema import validate
from jsonschema.exceptions import ValidationError
from jsonschema.exceptions import SchemaError

post_schema = {
    "type": "object",
    "properties": {
        "_id": {
            "type": "string"
        },
        "postType": {
            "type": "string"
        },
        "content": {
            "type": "object"
        },
        "comments": {
            "type": "array"
        },
        "hearts": {
            "type": "array"
        },
        "saves": {
            "type": "array"
        },
        "shares": {
            "type": "array"
        },
        "username": {
            "type": "string"
        },
        "firstName": {
            "type": "string"
        },
        "lastName": {
            "type": "string"
        },
        "timestamp": {
            "type": "string"
        },
        "profileImage": {
            "type": "string"
        }
    },
    "required": ["content", "firstName", "lastName", "username", "postType"],
    "additionalProperties": False
}

comment_schema = {
    "type": "object",
    "properties": {
        "content": {
            "type": "object"
        },
        "username": {
            "type": "string"
        },
        "profileImage": {
            "type": "string"
        },
        "_id": {
            "type": "string"
        }
    },
    "required": ["content", "username"],
    "additionalProperties": False
}


def validate_post(data):
    try:
        validate(data, post_schema)
    except ValidationError as e:
        return {'ok': False, "message": e}
    except SchemaError as e:
        return {'ok': False, "message": e}
    return {'ok': True, "data": data}


def validate_comment(data):
    try:
        validate(data, comment_schema)
    except ValidationError as e:
        return {'ok': False, "message": e}
    except SchemaError as e:
        return {'ok': False, "message": e}
    return {'ok': True, "data": data}
