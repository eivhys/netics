import React, { useEffect } from 'react'
import { Router, Switch, Route } from 'react-router-dom'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import refreshToken from './store/api/auth/refreshToken'
import fetchChat from './store/api/chat/fetchChat'
import history from "./helpers/history"
import Login from './pages/login/LoginWrapper'
import Register from './pages/register/RegisterWrapper'
import Home from './pages/home/Home'
import AuthRoute from './components/layout/AuthRoute'
import Bookmarks from './pages/bookmarks/Bookmarks'
import Setup from './components/post/Setup'
import 'antd/dist/antd.css'
import './App.css'
import Profile from './pages/profile/Profile'
import Messages from './pages/messages/Messages';
import LoginWrapper from './pages/login/LoginWrapper';
import NotImplemented from './pages/notimplemented/NotImplemented'

function App(props) {


  return (
    <Router history={history}>
      <Setup />
      <Switch>
        <AuthRoute rightPanel={true} exact={true} path="/home" component={Home} />
        <AuthRoute rightPanel={true} exact={true} path="/profile/:username" component={Profile} />
        <AuthRoute rightPanel={false} exact={true} path="/messages" component={Messages} />
        <AuthRoute rightPanel={false} exact={true} path="/messages/:username" component={Messages} />
        <AuthRoute rightPanel={true} exact={true} path="/bookmarks" component={Bookmarks} />
        <AuthRoute exact={true} path="/notimplemented" component={NotImplemented} />
        <Route exact={true} path="/test" component={LoginWrapper} />
        <Route exact={true} path="/login" component={Login} />
        <Route exact={true} path="/register" component={Register} />
        <AuthRoute rightPanel={true} exact={true} path="*" component={Home} />
      </Switch>
    </Router>
  )
}


const mapStateToProps = state => ({
  error: state.error,
  user: state.user,
  pending: state.pending,
  currentUser: state.currentUser
})

const mapDispatchToProps = dispatch => bindActionCreators({
  refreshToken: refreshToken,
  fetchChat: fetchChat
}, dispatch)

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(App)