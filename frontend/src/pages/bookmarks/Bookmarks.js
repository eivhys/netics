import React, { useState, useEffect } from 'react'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import createPost from '../../store/api/post/createPost'
import getPosts from '../../store/api/post/getPosts';
import deletePost from '../../store/api/post/deletePost';
import home from '../../style/Home.module.css'
import Feed from '../../components/post/Feed';
import { Icon } from 'antd'

const Home = (props) => {

    const { post, getPosts } = props

    let [page, setPage] = useState(0)

    useEffect(() => {
        // Fetch posts
        getPosts(`saved?page=${page}`, page)

        // Scroll listener
        window.addEventListener('scroll', infiteScroll);
        return () => window.removeEventListener('scroll', infiteScroll);
    }, [])

    const infiteScroll = () => {
        if ((window.innerHeight + window.scrollY) >= document.body.scrollHeight) {
            setPage(++page)
            getPosts(`saved?page=${page}`)
        }
    }

    return (
        <div>
            <div className={home.header}>
                <h1 className={home.title} style={{ fontSize: 24 }}><b>Bookmarks</b></h1>
            </div>
            {!!post.error  ? <Feed error divider={10} /> : <Feed divider={10} />}
        </div>
    );
}


const mapStateToProps = state => ({
    post: state.post,
    currentUser: state.currentUser
})

const mapDispatchToProps = dispatch => bindActionCreators({
    createPost: createPost,
    getPosts: getPosts,
    deletePost: deletePost
}, dispatch)

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(Home)