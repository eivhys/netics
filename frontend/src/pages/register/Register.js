import React, { useState, useEffect } from 'react'
import {
  Form,
  Input,
  Card,
  Button,
} from 'antd';
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import history from '../../helpers/history'
import registerUser from '../../store/api/auth/registerUser'
import loginUser from '../../store/api/auth/loginUser'
import form from '../../style/Forms.module.css'

const styles = {
  display: 'flex',
  justifyContent: 'center',
  alignItems: 'center',
  height: '100vh'
}

const RegistrationForm = (props) => {

  const [confirmDirty, setConfirmDirty] = useState(false)

  useEffect(() => {
    if (props.currentUser.user !== null) {
      history.push('/')
    }
  })

  const handleSubmit = event => {
    event.preventDefault();
    props.form.validateFieldsAndScroll((err, values) => {
      if (!err) {
        delete values.confirmPassword // deletes the second pw field
        props.registerUser({
          ...values,
          image: '',
          bio: '',
          followers: [],
          following: []
        })
      }
    })
  }

  const handleConfirmBlur = event => {
    const { value } = event.target;
    setConfirmDirty(confirmDirty || !!value)
  };

  const compareToFirstPassword = (rule, value, callback) => {
    const { form } = props;
    if (value && value !== form.getFieldValue('password')) {
      callback('Two passwords that you enter is inconsistent!');
    } else {
      callback();
    }
  };

  const validateToNextPassword = (rule, value, callback) => {
    const { form } = props;
    if (value && confirmDirty) {
      form.validateFields(['confirm'], { force: true });
    }
    callback();
  };

  const { getFieldDecorator } = props.form;

  const formItemLayout = {
    labelCol: {
      xs: { span: 24 },
      sm: { span: 8 },
    },
    wrapperCol: {
      xs: { span: 24 },
      sm: { span: 16 },
    },
  };
  const tailFormItemLayout = {
    wrapperCol: {
      xs: {
        span: 24,
        offset: 0,
      },
      sm: {
        span: 16,
        offset: 8,
      },
    },
  };

  return (
    <div className="form-background" style={styles}>
      <Card className='form-card' title={<h1 className={form.title}>Register</h1>} bordered={false} style={{ width: '35rem' }}>
        <Form {...formItemLayout} onSubmit={handleSubmit}>
          <Form.Item label={<b>E-mail</b>}>
            {getFieldDecorator('email', {
              rules: [
                {
                  type: 'email',
                  message: 'The input is not valid E-mail!',
                },
                {
                  required: true,
                  message: 'Input your E-mail!',
                },
              ],
            })(<Input />)}
          </Form.Item>
          <Form.Item
            label={
              <b>
                Username
              </b>
            }
          >
            {getFieldDecorator('username', {
              rules: [{ max: 16, pattern: "^[a-zA-Z0-9_]*$", required: true, message: 'Username can contain letters, numbers and underscore, and max 16 characters', whitespace: true }],
            })(<Input />)}
          </Form.Item>
          <Form.Item
            label={
              <b>
                First name
              </b>
            }
          >
            {getFieldDecorator('firstName', {
              rules: [{ required: true, message: 'Input your first name!', whitespace: true }],
            })(<Input />)}
          </Form.Item>
          <Form.Item
            label={
              <b>
                Last name
              </b>
            }
          >
            {getFieldDecorator('lastName', {
              rules: [{ required: true, message: 'Input your last name!', whitespace: true }],
            })(<Input />)}
          </Form.Item>
          <Form.Item label={<b>Password</b>} hasFeedback>
            {getFieldDecorator('password', {
              rules: [
                {
                  required: true,
                  message: 'Input your password!',
                },
                {
                  validator: validateToNextPassword,
                },
              ],
            })(<Input.Password />)}
          </Form.Item>
          <Form.Item label={<b>Confirm Password</b>} hasFeedback>
            {getFieldDecorator('confirmPassword', {
              rules: [
                {
                  required: true,
                  message: 'Confirm your password!',
                },
                {
                  validator: compareToFirstPassword,
                },
              ],
            })(<Input.Password onBlur={handleConfirmBlur} />)}
          </Form.Item>
          <Form.Item {...tailFormItemLayout}>
            <Button shape="round" type="primary" htmlType="submit">
              <b>Register</b>
            </Button>
          </Form.Item>
          <Form.Item {...tailFormItemLayout}>
            Already have an account? Sign in <a onClick={() => history.push('/login')}>here</a>
          </Form.Item>
        </Form>
      </Card>
    </div>
  );
}

const mapStateToProps = state => ({
  currentUser: state.currentUser
})

const mapDispatchToProps = dispatch => bindActionCreators({
  registerUser: registerUser,
  loginUser: loginUser
}, dispatch)

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Form.create({ name: 'register' })(RegistrationForm))