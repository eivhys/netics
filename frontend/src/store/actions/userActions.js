export const followUser = (follower, following) => ({
    type: 'FOLLOW_USER',
    follower,
    following
})

export const unfollowUser = (follower, following) => ({
    type: 'UNFOLLOW_USER',
    follower,
    following
})

export const getUser = () => ({
    type: 'USER'
})

export const FOLLOW_USER_PENDING = 'FOLLOW_USER_PENDING'
export const FOLLOW_USER_SUCCESS = 'FOLLOW_USER_SUCCESS'
export const FOLLOW_USER_FAILURE = 'FOLLOW_USER_FAILURE'

export const UNFOLLOW_USER_PENDING = 'UNFOLLOW_USER_PENDING'
export const UNFOLLOW_USER_SUCCESS = 'UNFOLLOW_USER_SUCCESS'
export const UNFOLLOW_USER_FAILURE = 'UNFOLLOW_USER_FAILURE'

export const GET_USER_PENDING = 'GET_USER_PENDING'
export const GET_USER_SUCCESS = 'GET_USER_SUCCESS'
export const GET_USER_FAILURE = 'GET_USER_FAILURE'

export const followUserPending = () => {
    return {
        type: FOLLOW_USER_PENDING
    }
}

export const followUserSuccess = (follower, following) => {
    return {
        type: FOLLOW_USER_SUCCESS,
        follower,
        following
    }
}

export const followUserFailure = (error) => {
    return {
        type: FOLLOW_USER_FAILURE,
        error
    }
}

export const unfollowUserPending = () => {
    return {
        type: UNFOLLOW_USER_PENDING
    }
}

export const unfollowUserSuccess = (follower, following) => {
    return {
        type: UNFOLLOW_USER_SUCCESS,
        follower,
        following
    }
}

export const unfollowUserFailure = (error) => {
    return {
        type: UNFOLLOW_USER_FAILURE,
        error
    }
}

export const getUserPending = () => {
    return {
        type: GET_USER_PENDING
    }
}

export const getUserSuccess = (user) => {
    return {
        type: GET_USER_SUCCESS,
        user
    }
}

export const getUserFailure = (error) => {
    return {
        type: GET_USER_FAILURE,
        error
    }
}