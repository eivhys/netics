import { updateUserPending, updateUserSuccess, updateUserFailure } from '../../actions/currentUserActions'
import refreshToken from '../auth/refreshToken'

const updateUser = (data) => {
    return dispatch => {
        dispatch(updateUserPending())
        fetch(`/api/v1/user`,
            {
                method: 'PATCH',
                headers: {
                    Authorization: `Bearer ${localStorage.getItem('accessToken')}`
                },
                body: JSON.stringify(data)
            }).then(res => res.json())
            .then(res => {
                if (res.error) {
                    throw (res.error)
                } else if (res.ok) {
                    dispatch(updateUserSuccess(res.data))
                } else {
                    dispatch(updateUserFailure(res))
                    dispatch(refreshToken(updateUser, { data }))
                }
            })
            .catch(error => {
                dispatch(updateUserFailure(error))
            })
    }
}

export default updateUser