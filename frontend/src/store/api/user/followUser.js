import { followUserPending, followUserSuccess, followUserFailure } from '../../actions/userActions'
import refreshToken from '../auth/refreshToken'


const followUser = (following, follower) => {
    return dispatch => {
        dispatch(followUserPending())
        fetch(`/api/v1/follow?user=${follower}`,
            {
                method: 'POST',
                headers: {
                    Accept: 'application/json',
                    Authorization: `Bearer ${localStorage.getItem('accessToken')}`
                },
            }).then(res => res.json())
            .then(res => {
                if (res.error) {
                    throw (res.error)
                } else if (res.ok) {
                    dispatch(followUserSuccess(following, follower))
                } else {
                    dispatch(followUserFailure(res))
                    dispatch(refreshToken(followUser, { followUser, follower }))
                }
            })
            .catch(error => {
                dispatch(followUserFailure(error))
            })
    }
}

export default followUser