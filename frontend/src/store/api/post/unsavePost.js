import { postUnsavePending, postUnsaveSuccess, postUnsaveFailure } from '../../actions/postActions'
import refreshToken from '../auth/refreshToken'


const createPost = (post) => {
    return dispatch => {
        dispatch(postUnsavePending())
        fetch(`/api/v1/save`,
            {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json',
                    Accept: 'application/json',
                    Authorization: `Bearer ${localStorage.getItem('accessToken')}`
                },
                body: JSON.stringify(post),
            }).then(res => res.json())
            .then(res => {
                if (res.error) {
                    throw (res.error)
                } else if (res.ok) {
                    dispatch(postUnsaveSuccess(post))
                } else {
                    dispatch(postUnsaveFailure())
                    dispatch(refreshToken(createPost, { post }))
                }
            })
            .catch(error => {
                dispatch(postUnsaveFailure(error))
            })
    }
}

export default createPost