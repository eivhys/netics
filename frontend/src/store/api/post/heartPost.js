import { postHeartPending, postHeartSuccess, postHeartFailure } from '../../actions/postActions'
import refreshToken from '../auth/refreshToken'

const createPost = (postId, user) => {
    return dispatch => {
        dispatch(postHeartPending())
        fetch(`/api/v1/heart?id=${postId}`,
            {
                method: 'POST',
                headers: {
                    Accept: 'application/json',
                    Authorization: `Bearer ${localStorage.getItem('accessToken')}`
                },
            }).then(res => res.json())
            .then(res => {
                if (res.error) {
                    throw (res.error)
                } else if (res.ok) {
                    dispatch(postHeartSuccess(postId, user))
                } else {
                    dispatch(postHeartFailure())
                    dispatch(refreshToken(createPost, { postId, user }))
                }
            })
            .catch(error => {
                dispatch(postHeartFailure(error))
            })
    }
}

export default createPost