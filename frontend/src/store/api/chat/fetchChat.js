import { fetchChatPending, fetchChatSuccess, fetchChatFailure } from '../../actions/chatActions'
import refreshToken from '../auth/refreshToken'
import messenger from '../../../style/Messenger.module.css'
import history from '../../../helpers/history';

const fetchChat = () => {
    return dispatch => {
        dispatch(fetchChatPending())
        fetch(`/messaging/inbox`,
            {
                method: 'GET',
                headers: {
                    'Content-Type': 'application/json',
                    Accept: 'application/json',
                    Authorization: `Bearer ${localStorage.getItem('accessToken')}`
                },
            }).then(res => res.json())
            .then(res => {
                if (res.error) {
                    throw (res.error)
                } else if (res.ok) {
                    dispatch(fetchChatSuccess(res.data))
                } else {
                    dispatch(fetchChatFailure())
                    dispatch(refreshToken(fetchChat, {}))
                }
            })
            .catch(error => {
                dispatch(fetchChatFailure(error))
            })
    }
}

export default fetchChat