import React, { useState, useEffect } from 'react'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import { Button, Row, Col, Input, Form, Icon } from 'antd'
import getUser from '../../store/api/user/getUser'
import home from '../../style/Home.module.css'
import messenger from '../../style/Messenger.module.css'
import post from '../../style/Post.module.css'
import cx from 'classnames'
import TextArea from 'antd/lib/input/TextArea'
import history from '../../helpers/history'
import sendMessage from '../../store/api/chat/sendMessage'
import MessagesLoading from './MessagesLoading'

const Messages = (props) => {

    const { currentUser, params, params: { username }, user, chat, message } = props
    const [draft, setDraft] = useState('')
    const limit = 1024

    useEffect(() => {
        const textArea = document.getElementsByClassName(messenger.chatTextArea)[0]
        textArea.style.height = ""
        textArea.style.height = `${50}px`

        if (params.username === undefined && message.inbox.length > 0) {
            const firstInbox = message.inbox[0].users.filter(u => u !== currentUser.user.username)[0]
            history.push(`/messages/${firstInbox}`)
            console.log("first inbox: " + firstInbox)
        }
    }, [])

    /**
     * 
     * @param {event} e 
     * Adjusts height of post input, updates cursor position,
     * and adds input to post
     */
    const handlePostChange = (e) => {
        const textArea = document.getElementsByClassName(messenger.chatTextArea)[0]
        textArea.style.height = ""
        textArea.style.height = `${Math.min(textArea.scrollHeight, limit)}px`
        //setCursor(cursor + e.length - content.length)
        setDraft(e.target.value)
    }

    const handleKeyDown = (e) => {
        console.log(e.key)
        if (e.key === "Enter" && draft.length > 0) {
            if (e.nativeEvent.shiftKey) {
                setDraft(e.target.value)
            } else {
                e.preventDefault()
                props.sendMessage(chat._id, draft)
                setDraft('')
            }
        }
    }


    return (
        <div style={{ height: '100vh' }}>
            <Row type="flex" justify="space-between" style={{ height: '100%' }}>
                <Col className={messenger.chat}>
                    <div className={cx([home.header, messenger.header])} id="chatHeader" style={{ width: '100%', height: 'min-content' }} >
                        <h1 className={home.title} style={{ fontSize: 24 }}><b>{!username ? "Messages" : `@${username}`}</b></h1>
                    </div>
                    {
                        chat === undefined || currentUser.user === null ? <MessagesLoading /> : <div className={messenger.messages}>
                            <div style={{ width: '100%', display: 'flex', flexDirection: 'column', bottom: 0 }} >
                                {
                                    chat.messages.map(message =>
                                        <div key={'' + message._id} style={{ width: '100%' }}>
                                            <div className={message.sender === currentUser.user.username ? messenger.sent : messenger.received}>
                                                {message.text}
                                            </div>
                                        </div>)
                                }
                            </div>
                            <br />
                        </div>
                    }
                    <div className={messenger.inputContainer}>
                        <TextArea disabled={(!username || user.error)} type="text" value={draft} className={messenger.chatTextArea} onKeyPress={handleKeyDown} onChange={handlePostChange} />
                        <Button disabled={draft.length === 0 || (!username || user.error)} type="primary" onClick={() => {
                            props.sendMessage(chat._id, draft)
                            setDraft('')
                        }} shape="round">Send</Button>
                    </div>
                </Col>
            </Row>
        </div>
    );
}


const mapStateToProps = state => ({
    currentUser: state.currentUser,
    user: state.user,
    message: state.message,
})

const mapDispatchToProps = dispatch => bindActionCreators({
    getUser: getUser,
    sendMessage: sendMessage,
}, dispatch)

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(Messages)