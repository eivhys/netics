import React from 'react'
import Skeleton from 'react-loading-skeleton';


const AvatarLoading = (props) => {

    return <Skeleton circle={true} width={50} height={50} />
}

export default AvatarLoading