import React, { useState } from 'react'
import { Icon } from 'antd';
import cx from 'classnames'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import heartPost from '../../store/api/post/heartPost'
import unheartPost from '../../store/api/post/unheartPost'
import bookmarkPost from '../../store/api/post/bookmarkPost'
import unBookmarkPost from '../../store/api/post/unbookmarkPost'
import post from '../../style/Post.module.css'
import CreateComment from './CreateComment';


const BottomButtons = (props) => {

    const [commentModal, setCommentModal] = useState(false)
    const { data, currentUser } = props

    const closeComments = () => {
        setCommentModal(false)
    }

    const openComments = () => {
        setCommentModal(true)
    }

    const includesCurrentUser = (list) => {
        if (!!!currentUser.user) {
            return false
        }
        return list.includes(currentUser.user.username)
    }

    return (
        <div className={post.bottomBar}>
            <CreateComment show={commentModal} data={data} close={closeComments} />
            <h2
                className={cx(post.bottomButton, post.bottomComment)}
                onClick={() => openComments()}
            >
                <Icon type="message" /> {data.comments.length}</h2>
            <h2
                className={cx(post.bottomButton, post.bottomHeart)}
                onClick={includesCurrentUser(data.hearts) ?
                    () => props.unheartPost(data._id, currentUser.user.username) :
                    () => props.heartPost(data._id, currentUser.user.username)}
            >
                <Icon type="heart" theme={includesCurrentUser(data.hearts) ? "filled" : "outlined"} /> {data.hearts.length}</h2>
            {/*<h2
                className={cx(post.bottomButton, post.bottomShare)}
            >
            <Icon type="sync" /> {data.shares.length}</h2 >*/}
            <h2
                className={cx(post.bottomButton, post.bottomBookmark)}
                onClick={
                    includesCurrentUser(data.saves) ?
                        () => props.unBookmarkPost(data._id, currentUser.user.username) :
                        () => props.bookmarkPost(data._id, currentUser.user.username)
                }
            >
                <Icon
                    theme={includesCurrentUser(data.saves) ? "filled" : "outlined"}
                    type="book" />
            </h2>
        </div >
    );
}


const mapStateToProps = state => ({
    post: state.post,
    currentUser: state.currentUser
})

const mapDispatchToProps = dispatch => bindActionCreators({
    heartPost: heartPost,
    unheartPost: unheartPost,
    bookmarkPost: bookmarkPost,
    unBookmarkPost: unBookmarkPost
}, dispatch)

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(BottomButtons)