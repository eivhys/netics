import React from 'react'
import file from '../../assets/Profile.jpg'

const ProfileImage = (props) => {
    return props.src ? <img className={props.className} alt="" src={props.src} /> : <img className={props.className} alt="" src={file} />
}

export default ProfileImage