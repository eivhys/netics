import React from 'react'
import { Input, Typography, List} from 'antd'
import cx from 'classnames'
import trending from '../../../style/RightPanel.module.css'

const Trending = (props) => {

    const data = [
        'Hei',
        'Test',
        'Hashtag',
      ];

    return (
        <List
        className={trending.trendingBox}
      header={<div><b>Trending topics</b></div>}
      footer={null}
      bordered
      
      dataSource={data}
      renderItem={(item, i) => (
        <List.Item className={cx(i === data.length - 1 ? [trending.trending, trending.trendingLast] : [trending.trending] )}>
          <p>#{item}</p>
        </List.Item>
      )}
    />
    );
}

export default Trending